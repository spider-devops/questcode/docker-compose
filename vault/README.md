#######
mkdir -p volumes/{config,file,logs}

######
cat > volumes/config/vault.json << EOF
{
  "backend": {
    "file": {
      "path": "/vault/file"
    }
  },
  "listener": {
    "tcp":{
      "address": "0.0.0.0:8200",
      "tls_disable": 1
    }
  },
  "ui": true
}
EOF

#######
vault operator init -key-shares=6 -key-threshold=3

vault operator unseal
vault operator unseal
vault operator unseal

vault login



###########
vault secrets enable -version=1 -path=secret kv


#######
vault kv put secret/jenkins/dockerhub/credentials username=<USER_ID>  password=<PASSWORD_ID>

